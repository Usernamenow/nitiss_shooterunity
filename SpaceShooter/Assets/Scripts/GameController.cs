﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	public GameObject hazard;
	public GameObject loot;
	public GameObject station;

	public GUIText scoreText;
	public int Loot;

	public GUIText cargoText;

	public Vector3 spawnValues;

	public int lootCount;


	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;

	public float lootWait;

	void Start ()
	{
		StartCoroutine (SpawnWaves ());
		StartCoroutine (SpawnCargo ());
	}



///	void start()
///	{
///		loot = 0;
///		UpdateLoot ();
///		StartCoroutine (SpawnWaves ());

///	}




	IEnumerator SpawnWaves ()
	{
		yield return new WaitForSeconds (startWait);
		while (true)
		{
			for (int i = 0;i < hazardCount; i++) 
			{
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (hazard, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);
			}

			yield return new WaitForSeconds (waveWait/2f);
			Vector3 stationPosition = new Vector3 (0f, spawnValues.y, spawnValues.z);
			Quaternion stationRotation = Quaternion.identity;
			Instantiate (station, stationPosition, stationRotation);
			yield return new WaitForSeconds (waveWait / 2f);
		}
	

	
	}
		








	IEnumerator SpawnCargo ()
	{
		yield return new WaitForSeconds (startWait);
		while (true) {
			for (int i = 0; i < lootCount; i++) {
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (loot, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (lootWait);
			}
			yield return new WaitForSeconds (waveWait);
		}




	}


	void UpdateCargo()
	{
		cargoText.text = "Loot: " + loot;

	}






	//	void UpdateScore()

	//{
	//	scoreText.text = "Score: " + score;
	//}

}
