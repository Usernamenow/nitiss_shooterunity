﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
	public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour 
{ 
	public GUIText cargoText;
	//public int cargo


	public float speed;
	public float tilt;
	public Boundary boundary;

	public int score = 0;

	public GameObject shot; 
	public Transform shotSpawn;
	public float fireRate;

	public int health; 

	public GameObject station;

	public int loot = 0;

	public GameObject playerExplosion;

	public GameObject discard;

	private float nextFire;

	private Rigidbody rb;









	//void Update ()
	//{
	//	if (Input.GetButton("Fire1") && Time.time > nextFire)
	//	{
	//		nextFire = Time.time + fireRate;
			//GameObject clone =
	//		Instantiate (shot, shotSpawn.position, shotSpawn.rotation); // as GameObject;
	//	}
	//}

	void Start()
	{
		rb = GetComponent<Rigidbody> ();
	}
		

	public void Box (int box)
	{
		
		if (loot < 8)
			loot += box;


	}




	void Update()
	{

		if (Input.GetButtonDown ("Fire1")) 
		{
			if (loot >= 1) 
			{
				loot -= 1;
				Instantiate (discard, transform.position, Quaternion.identity);
			}
			print (loot);

		}
	}




	public void Station (int station)
	{
		if (loot >= station)
		{
			score += loot * 2;
			loot -= loot;
			print (loot);	
		}
	}








	//public void Addcargo (int newLootValue)
	//{
		//loot + newLootValue;
	///	Updateloot ();
	
	//}


	void UpdateCargo()
	{
		cargoText.text = "Loot: " + loot;
	
	}




	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rb.velocity = movement * speed * Mathf.Clamp(((9 - loot)/9f),.1f,1f);

		rb.position = new Vector3 
		(
			Mathf.Clamp (rb.position.x, boundary.xMin, boundary.xMax), 
			0.0f, 
			Mathf.Clamp (rb.position.z, boundary.zMin, boundary.zMax)
		);

		rb.rotation = Quaternion.Euler (0.0f, 0.0f, rb.velocity.x * -tilt);
	
	}
		




	public void Damage (int damage)
	{

		health -= damage;


		if (health <= 0) 
		{
			Instantiate (playerExplosion, transform.position, transform.rotation);
			Destroy (gameObject);
		}

	}


}
