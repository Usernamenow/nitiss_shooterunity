﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroybyContact : MonoBehaviour 
{
	public GameObject explosion;
	public GameObject playerExplosion;

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Boundary" || other.tag == "Loot")  
		{
			return;
		}
	
		Instantiate (explosion, transform.position, transform.rotation);
		if (other.tag == "Player") {
			//Instantiate (playerExplosion, other.transform.position, other.transform.rotation);
			other.GetComponent < PlayerController > ().Damage (1);
		}
		else 
			{
			Destroy (other.gameObject);
		}

		Destroy(gameObject);
	}
}

